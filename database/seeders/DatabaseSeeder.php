<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Alarm;
use App\Models\Event;
use App\Models\Installation;
use App\Models\InstallationEvent;
use App\Models\User;
use Faker\Core\Number;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Populating Users, Installations and Events
        User::factory(50)->create();
        Installation::factory(30)->create();
        Event::factory(10)->create();

        // Get all the users attaching up to 3 random users to each installation
        $users = User::all();
        $events = Event::all();

        // Populate the pivot table
        Installation::all()->each(function ($installation) use ($users, $events) {
            $installation->users()->attach(
                $users->random(rand(1, 3))->pluck('id')->toArray()
            );
            $installation->events()->attach(
                $events->random(rand(1, 3))->pluck('id')->toArray()
            );
        });

        // Populating Alarms
        Alarm::factory(100)->create();
    }
}
