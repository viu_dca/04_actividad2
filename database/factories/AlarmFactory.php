<?php

namespace Database\Factories;

use App\Models\InstallationEvent;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Event>
 */
class AlarmFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'description' => fake()->text(),
            'status' => ['pending', 'processed', 'rejected'][rand(0, 2)],
            'installation_event_id' => InstallationEvent::all()->random(1)->pluck('id')->first()
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            //
        ]);
    }
}
