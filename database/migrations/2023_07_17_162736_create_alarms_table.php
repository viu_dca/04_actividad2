<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('alarms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('installation_event_id')->unsigned();
            $table->string('description')->nullable();
            $table->string('status')->default('pending');
            $table->timestamps();
            $table->foreign('installation_event_id')->references('id')
                ->on('installation_events')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('alarms');
    }
};
