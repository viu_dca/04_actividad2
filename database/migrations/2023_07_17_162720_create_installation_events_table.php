<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('installation_events', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(true);
            $table->integer('installation_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->timestamps();
            $table->foreign('installation_id')->references('id')
                ->on('installations')
                ->onDelete('cascade');
            $table->foreign('event_id')->references('id')
                ->on('events')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('installation_events');
    }
};
