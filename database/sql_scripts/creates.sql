CREATE TABLE Users (
	dni VARCHAR(9)    PRIMARY KEY NOT NULL,
	correo          VARCHAR(50)   NOT NULL, 
	nombre_usuario  VARCHAR(100)  NOT NULL,
	apellidos       VARCHAR(100)  NOT NULL, 
	telefono_movil  VARCHAR(9)    NOT NULL, 
	fecha_nacimiento  DATE        NOT NULL, 
	contrasena      VARCHAR(15)   NOT NULL, 
	rol             VARCHAR(50)    NOT NULL,
	es_titular      boolean       NOT NULL
);

CREATE  TABLE Installations (
	codigo_instalacion  VARCHAR(10)   PRIMARY KEY NOT NULL, 
	direccion           VARCHAR(100)  NOT NULL, 
	ciudad              VARCHAR(50)   NOT NULL, 
	telefono_fijo       VARCHAR(9)            , 
	nombre_instalacion  VARCHAR(100)           , 
	fecha_instalacion   DATE          NOT NULL
);

CREATE TABLE EVENTO ( 
	codigo_evento VARCHAR(10) PRIMARY KEY NOT NULL, 
	observaciones VARCHAR(200)            , 
	prioridad     VARCHAR(10)     NOT NULL,
	subevento     VARCHAR(100)             
);

CREATE TABLE CONTRATO ( 
	dni VARCHAR(9) NOT NULL,
	codigo_instalacion VARCHAR(10) NOT NULL, 
	PRIMARY KEY (dni, codigo_instalacion), 
	CONSTRAINT  fk_dni FOREIGN KEY(dni) REFERENCES USUARIO(dni),
	CONSTRAINT  fk_instalacion FOREIGN KEY(codigo_instalacion) REFERENCES INSTALACION(codigo_instalacion)
);

CREATE TABLE INSTALACION_EVENTO ( 
	id_instalacion_evento  VARCHAR(10)  NOT NULL, 
	codigo_instalacion  VARCHAR(10)  NOT NULL, 
	codigo_evento       VARCHAR(10)  NOT NULL, 
	activo              BOOLEAN      NOT NULL, 
	PRIMARY KEY (id_instalacion_evento), 
	CONSTRAINT  fk_codigo_instalacion FOREIGN KEY (codigo_instalacion) REFERENCES INSTALACION(codigo_instalacion),
	CONSTRAINT  fk_codigo_evento FOREIGN KEY(codigo_evento) REFERENCES EVENTO(codigo_evento)
);

CREATE TABLE ALARMA ( 
	id_alarma           VARCHAR(10)     PRIMARY KEY    NOT NULL, 
	id_instalacion_evento  VARCHAR(10)  NOT NULL, 
	observaciones       VARCHAR(100)    NOT NULL, 
	estado_notificacion VARCHAR(10)     NOT NULL, 
	fecha               DATE            NOT NULL, 
	CONSTRAINT  fk_id_instalacion_evento FOREIGN KEY(id_instalacion_evento) REFERENCES INSTALACION_EVENTO(id_instalacion_evento)
);