
## Instrucciones iniciales

Esta guía servirá de ayuda en simples pasos para levantar el proyecto **una vez tengamos clonado el repositorio**. Las instrucciones y comandos que se brindarán están orientadas a sistemas **Windows**.

### Requisitor previos

- Docker Desktop
- Distribución de Ubuntu
- WSL 2
- Composer
- PHP

### Siguientes pasos

1. Abrir una terminal en la raíz del proyecto.
2. Ejecutar comandos:
```
composer update --ignore-platform-reqs
copy .env.example .env
php artisan key:generate

```
3. Modificar las variables de entorno [DB_]:
```
DB_CONNECTION=pgsql
DB_HOST=pgsql
DB_PORT=5432
DB_DATABASE=04_actividad2
DB_USERNAME=sail
DB_PASSWORD=password
```
4. Ejecutar comandos:
```
wsl
./vendor/bin/sail up -d
./vendor/bin/sail artisan migrate
./vendor/bin/sail artisan db:seed
```

Terminando estos pasos podrá acceder a la página inicial para comprobar que está ejecutandose la aplicación por [localhost](localhost).

Para acceder a los servicios implementados puede usar la colección de Postman incluída en el proyecto [04_actividad2.postman_collection](/postman/04_actividad2.postman_collection.json)
