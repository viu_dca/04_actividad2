<?php

use App\Http\Controllers\AlarmController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\InstallationController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(UserController::class)->prefix('users')->group(function () {
    Route::get('/', 'getList');
    Route::get('/search/', 'search');
    Route::get('/{user}', 'getOne');
    Route::post('/', 'store');
    Route::post('/{user}', 'update');
    Route::put('/{user}', 'put');
    Route::delete('/{user}', 'destroy');
});

Route::controller(InstallationController::class)->prefix('installations')->group(function () {
    Route::get('/', 'getList');
    Route::get('/search/', 'search');
    Route::get('/{installation}', 'getOne');
    Route::post('/', 'store');
    Route::post('/{installation}', 'update');
    Route::put('/{installation}', 'put');
    Route::delete('/{installation}', 'destroy');
});

Route::controller(EventController::class)->prefix('events')->group(function () {
    Route::get('/', 'getList');
    Route::get('/search/', 'search');
    Route::get('/{event}', 'getOne');
    Route::post('/', 'store');
    Route::post('/{event}', 'update');
    Route::put('/{event}', 'put');
    Route::delete('/{event}', 'destroy');
});

Route::controller(AlarmController::class)->prefix('alarms')->group(function () {
    Route::get('/', 'getList');
    Route::get('/search/', 'search');
    Route::put('/{alarm}', 'put');
});