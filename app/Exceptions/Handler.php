<?php

namespace App\Exceptions;

use App\Http\Middleware\ResultResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (NotFoundHttpException $e, Request $request) {
            $response = new ResultResponse();

            // Handing error response
            if (
                $request->is('api/v1/users/*')
                || $request->is('api/v1/installations/*')
                || $request->is('api/v1/events/*')
                || $request->is('api/v1/alarms/*')
            ) {
                $response->setStatusCode(ResultResponse::ERROR_ELEMENT_NOT_FOUND_CODE);
                $response->setMessage(ResultResponse::TXT_ERROR_ELEMENT_NOT_FOUND_CODE);
                $response->setData((object)['error_message' => $e->getMessage()]);
            }

            return response()->json($response, $response->getStatusCode());
        });
    }
}
