<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed $users
 * @property mixed $events
 * @property mixed $alarms
 */
class Installation extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'address',
        'city',
        'phone',
        'name',
        'installation_date'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'installation_date' => 'datetime'
    ];

    /**
     * The users that belong to the installation.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'contracts')
            ->withTimestamps();
    }

    /**
     * The events that belong to the installation.
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'installation_events')
            ->withTimestamps();
    }

    /**
     * The alarms that belong to the installation.
     */
    public function alarms(): HasManyThrough
    {
        return $this->hasManyThrough(Alarm::class, InstallationEvent::class);
    }

    public const VALIDATION_RULES = [
        'address' => ['required'],
        'city' => ['required'],
        'name' => ['required'],
        'phone' => ['required'],
        'installation_date' => ['required', 'date'],
        'users' => ['required'],
        'events' => ['required']
    ];
    public const VALIDATION_MESSAGES = [
        'required' => 'The :attribute field is required.',
        'unique' => 'The :attribute field must not be duplicated.',
        'size' => 'The :attribute must be exactly :size.',
        'email' => 'The :attribute must be valid.',
        'boolean' => 'The :attribute must be a valid true/false value.',
    ];
    public const SEARCH_COLUMNS = [
        'address',
        'city',
        'phone',
        'name'
    ];
}
