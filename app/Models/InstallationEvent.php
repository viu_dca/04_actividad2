<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InstallationEvent extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'active'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        //
    ];

    /**
     * The installation that belong to the alarm.
     */
    public function installation(): BelongsTo
    {
        return $this->belongsTo(Installation::class);
    }

    /**
     * The event that belong to the alarm.
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    public const VALIDATION_RULES = [
        'active' => ['required']
    ];
    public const VALIDATION_MESSAGES = [
        'required' => 'The :attribute field is required.'
    ];
    public const SEARCH_COLUMNS = [
        'active'
    ];
}
