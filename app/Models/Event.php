<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'priority'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        //
    ];

    /**
     * The users that belong to the installation.
     */
    public function installations()
    {
        return $this->belongsToMany(Installation::class, 'installation_events');
    }

    public const VALIDATION_RULES = [
        'name' => ['required', 'unique:events'],
        'priority' => ['required']
    ];
    public const VALIDATION_MESSAGES = [
        'required' => 'The :attribute field is required.',
        'unique' => 'The :attribute field must not be duplicated.'
    ];
    public const SEARCH_COLUMNS = [
        'name',
        'description',
        'priority'
    ];
}
