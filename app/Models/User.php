<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property mixed $installations
 */
class User extends Model
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'dni',
        'first_name',
        'last_name',
        'email',
        'password',
        'birth_date',
        'cellphone',
        'rol',
        'is_holder',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'birth_date' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * The installations that belong to the user.
     */
    public function installations()
    {
        return $this->belongsToMany(Installation::class, 'contracts');
    }

    public const VALIDATION_RULES = [
        'dni' => ['required', 'unique:users', 'size:9'],
        'first_name' => ['required'],
        'last_name' => ['required'],
        'email' => ['required', 'email:rfc,dns'],
        'password' => ['required'],
        'birth_date' => ['required', 'date'],
        'cellphone' => ['required'],
        'rol' => ['required'],
        'is_holder' => ['required', 'boolean']
    ];
    public const VALIDATION_MESSAGES = [
        'required' => 'The :attribute field is required.',
        'unique' => 'The :attribute field must not be duplicated.',
        'size' => 'The :attribute must be exactly :size.',
        'email' => 'The :attribute must be valid.',
        'boolean' => 'The :attribute must be a valid true/false value.',
    ];
    public const SEARCH_COLUMNS = [
        'dni',
        'first_name',
        'last_name',
        'email',
        'cellphone'
    ];
}
