<?php

namespace App\Http\Controllers;

use App\Http\Middleware\ResultResponse;
use App\Models\Event;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Throwable;

class EventController extends Controller
{
    /**
     * Return a list of the resource.
     */
    public function getList(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get events data with pagination
            $events = Event::paginate($request->query('limit'));

            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($events as $key => $event){
                    $events[$key]['installations'] = $event->installations;
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($events);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Display the specified resource.
     */
    public function getOne(Request $request, Event $event): JsonResponse
    {
        $resultResponse = new ResultResponse();

        if($event->exists()) {
            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                $event['installations'] = $event->installations;
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($event);
        } else {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_ELEMENT_NOT_FOUND_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_ELEMENT_NOT_FOUND_CODE);
        }

        return response()->json($resultResponse);
    }

    /**
     * Return a list of the resource.
     */
    public function search(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get filtered events data with pagination
            $searchQuery = Event::query();
            foreach(Event::SEARCH_COLUMNS as $column){
                $searchQuery->orWhere($column, 'ILIKE', '%' . $request->query('q') . '%');
            }

            $events = $searchQuery->paginate($request->query('limit'));

            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($events as $key => $event){
                    $events[$key]['installations'] = $event->installations;
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($events);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                Event::VALIDATION_RULES,
                Event::VALIDATION_MESSAGES
            );
            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Creating Event
            $event = new Event($request->all());

            if($event->saveOrFail()) {
                // If creation success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($event);
            } else {
                // If creation fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Event $event): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                Event::VALIDATION_RULES,
                Event::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating Event
            if($event->exists() && $event->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($event);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Partially update the specified resource in storage.
     */
    public function put(Request $request, Event $event): JsonResponse
    {
        $resultResponse = new ResultResponse();

        // Filtering specific rules for PUT params
        $rules = array_filter(
            Event::VALIDATION_RULES,
            fn ($key) => $request->input($key) !== null,
            ARRAY_FILTER_USE_KEY
        );

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                $rules,
                Event::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating Event
            if($event->exists() && $event->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($event);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Event $event): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Removing Event
            if($event->exists() && $event->deleteOrFail()) {
                // If delete success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($event);
            } else {
                // If delete fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }
}
