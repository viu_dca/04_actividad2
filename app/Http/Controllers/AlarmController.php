<?php

namespace App\Http\Controllers;

use App\Http\Middleware\ResultResponse;
use App\Models\Alarm;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Throwable;

class AlarmController extends Controller
{
    /**
     * Return a list of the resource.
     */
    public function getList(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get alarms data with pagination
            $alarms = Alarm::paginate($request->query('limit'));

            // Seed Installation if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($alarms as $key => $alarm){
                    $alarms[$key]['installation'] = DB::table('alarms')
                        ->join('installation_events', 'alarms.installation_event_id', '=', 'installation_events.id')
                        ->join('installations', 'installation_events.installation_id', '=', 'installations.id')
                        ->where('alarms.id', '=', $alarm['id'])
                        ->where('alarms.installation_event_id', '=', $alarm['installation_event_id'])
                        ->select('installations.*')
                        ->first();
                    $alarms[$key]['event'] = DB::table('alarms')
                        ->join('installation_events', 'alarms.installation_event_id', '=', 'installation_events.id')
                        ->join('events', 'installation_events.event_id', '=', 'events.id')
                        ->where('alarms.id', '=', $alarm['id'])
                        ->where('alarms.installation_event_id', '=', $alarm['installation_event_id'])
                        ->select('events.*')
                        ->first();
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($alarms);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode($e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)['trace'=>$e->getTrace()]);
        }

        return response()->json($resultResponse);
    }

    /**
     * Return a list of the resource.
     */
    public function search(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get filtered alarms data with pagination
            $searchQuery = Alarm::query();
            foreach(Alarm::SEARCH_COLUMNS as $column){
                $searchQuery->orWhere($column, 'ILIKE', '%' . $request->query('q') . '%');
            }

            $alarms = $searchQuery->paginate($request->query('limit'));

            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($alarms as $key => $alarm){
                    $alarms[$key]['installation'] = DB::table('alarms')
                        ->join('installation_events', 'alarms.installation_event_id', '=', 'installation_events.id')
                        ->join('installations', 'installation_events.installation_id', '=', 'installations.id')
                        ->where('alarms.id', '=', $alarm['id'])
                        ->where('alarms.installation_event_id', '=', $alarm['installation_event_id'])
                        ->select('installations.*')
                        ->first();
                    $alarms[$key]['event'] = DB::table('alarms')
                        ->join('installation_events', 'alarms.installation_event_id', '=', 'installation_events.id')
                        ->join('events', 'installation_events.event_id', '=', 'events.id')
                        ->where('alarms.id', '=', $alarm['id'])
                        ->where('alarms.installation_event_id', '=', $alarm['installation_event_id'])
                        ->select('events.*')
                        ->first();
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($alarms);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Partially update the specified resource in storage.
     */
    public function put(Request $request, Alarm $alarm): JsonResponse
    {
        $resultResponse = new ResultResponse();

        // Filtering specific rules for PUT params
        $rules = array_filter(
            Alarm::VALIDATION_RULES,
            fn ($key) => $request->input($key) !== null,
            ARRAY_FILTER_USE_KEY
        );
        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                $rules,
                Alarm::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating Alarm
            if($alarm->exists() && $alarm->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($alarm);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }
}
