<?php

namespace App\Http\Controllers;

use App\Http\Middleware\ResultResponse;
use Hamcrest\Type\IsBoolean;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Throwable;
use function Webmozart\Assert\Tests\StaticAnalysis\boolean;

class UserController extends Controller
{

    /**
     * Return a list of the resource.
     */
    public function getList(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get users data with pagination
            $users = User::orderBy('id')->paginate($request->query('limit'));

            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($users as $key => $user){
                    $users[$key]['installations'] = $user->installations;
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($users);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Display the specified resource.
     */
    public function getOne(Request $request, User $user): JsonResponse
    {
        $resultResponse = new ResultResponse();

        if($user->exists()) {
            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                $user['installations'] = $user->installations;
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($user);
        } else {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_ELEMENT_NOT_FOUND_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_ELEMENT_NOT_FOUND_CODE);
        }

        return response()->json($resultResponse);
    }

    /**
     * Return a list of the resource.
     */
    public function search(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get filtered users data with pagination
            $searchQuery = User::query();
            foreach(User::SEARCH_COLUMNS as $column){

                $searchQuery->orWhere($column, 'ILIKE', '%' . $request->query('q') . '%');
            }

            if($request->query('with_trashed') == 'true') {
                $searchQuery->withTrashed();
            }

            $users = $searchQuery->orderBy('id')->paginate($request->query('limit'));

            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($users as $key => $user){
                    $users[$key]['installations'] = $user->installations;
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($users);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                User::VALIDATION_RULES,
                User::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Creating User
            $user = new User($request->all());

            if($user->saveOrFail()) {
                // If creation success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($user);
            } else {
                // If creation fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                User::VALIDATION_RULES,
                User::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating User
            if($user->exists() && $user->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($user);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Partially update the specified resource in storage.
     */
    public function put(Request $request, User $user): JsonResponse
    {
        $resultResponse = new ResultResponse();

        // Filtering specific rules for PUT params
        $rules = array_filter(
            User::VALIDATION_RULES,
            fn ($key) => $request->input($key) !== null,
            ARRAY_FILTER_USE_KEY
        );

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                $rules,
                User::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating User
            if($user->exists() && $user->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($user);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Removing User
            if($user->exists() && $user->deleteOrFail()) {
                // If delete success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($user);
            } else {
                // If delete fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }
}
