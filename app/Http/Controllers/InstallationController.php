<?php

namespace App\Http\Controllers;

use App\Http\Middleware\ResultResponse;
use App\Models\Event;
use App\Models\Installation;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Throwable;

class InstallationController extends Controller
{
    /**
     * Return a list of the resource.
     */
    public function getList(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get installations data with pagination
            $installations = Installation::orderBy('id')->paginate($request->query('limit'));

            // Seed Users if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($installations as $key => $installation){
                    $installations[$key]['users'] = $installation->users;
                    $installation['events'] = $installation->events;
                    $installation['alarms'] = $installation->alarms;
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($installations);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Display the specified resource.
     */
    public function getOne(Request $request, Installation $installation): JsonResponse
    {
        $resultResponse = new ResultResponse();

        if($installation->exists()) {
            // Seed Entities if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                $installation['users'] = $installation->users;
                $installation['events'] = $installation->events;
                $installation['alarms'] = $installation->alarms;
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($installation);
        } else {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_ELEMENT_NOT_FOUND_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_ELEMENT_NOT_FOUND_CODE);
        }

        return response()->json($resultResponse);
    }

    /**
     * Return a list of the resource.
     */
    public function search(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Get filtered installations data with pagination
            $searchQuery = Installation::query();
            foreach(Installation::SEARCH_COLUMNS as $column){
                $searchQuery->orWhere($column, 'ILIKE', '%' . $request->query('q') . '%');
            }

            if($request->query('with_trashed') == 'true') {
                $searchQuery->withTrashed();
            }

            $installations = $searchQuery->orderBy('id')->paginate($request->query('limit'));

            // Seed Installations if parameter 'seed' is true
            if($request->query('seed') == 'true') {
                foreach($installations as $key => $installation){
                    $installations[$key]['users'] = $installation->users;
                }
            }

            // Setting response data
            $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
            $resultResponse->setData($installations);
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
            $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                Installation::VALIDATION_RULES,
                Installation::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Validating users to assign
            $usersIds = (Array)$request->get('users');
            $users = User::find($usersIds);
            // Validating events to assign
            $eventsIds = (Array)$request->get('events');
            $events = Event::find($eventsIds);
            if(count($usersIds) != count($users) || count($eventsIds) != count($events)) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => 'Some related entity may not exist. Check on Users and Events ids.']);
                return response()->json($resultResponse);
            }

            // Creating Installation
            $installation = new Installation($request->all());

            if($installation->saveOrFail()) {
                // Attach users and events to installation
                $installation->users()->attach($users->pluck('id')->toArray());
                $installation->events()->attach($events->pluck('id')->toArray());
                // If creation success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($installation);
            } else {
                // If creation fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Installation $installation): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                Installation::VALIDATION_RULES,
                Installation::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating Installation
            if($installation->exists() && $installation->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($installation);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Partially update the specified resource in storage.
     */
    public function put(Request $request, Installation $installation): JsonResponse
    {
        $resultResponse = new ResultResponse();

        // Filtering specific rules for PUT params
        $rules = array_filter(
            Installation::VALIDATION_RULES,
            fn ($key) => $request->input($key) !== null,
            ARRAY_FILTER_USE_KEY
        );

        try {
            // Validating
            $validator = Validator::make(
                $request->all(),
                $rules,
                Installation::VALIDATION_MESSAGES
            );

            // If fail validations
            if($validator->fails()) {
                $resultResponse->setStatusCode(ResultResponse::ERROR_BAD_REQUEST_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_BAD_REQUEST_CODE);
                $resultResponse->setData((object)['errors' => $validator->messages()]);
                return response()->json($resultResponse);
            }

            // Updating Installations
            if($installation->updateOrFail($request->all())) {
                // If update success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($installation);
            } else {
                // If update fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Installation $installation): JsonResponse
    {
        $resultResponse = new ResultResponse();

        try {
            // Removing installation
            if($installation->exists() && $installation->deleteOrFail()) {
                // If delete success
                $resultResponse->setStatusCode(ResultResponse::SUCCESS_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_SUCCESS_CODE);
                $resultResponse->setData($installation);
            } else {
                // If delete fails
                $resultResponse->setStatusCode(ResultResponse::ERROR_CODE);
                $resultResponse->setMessage(ResultResponse::TXT_ERROR_CODE);
            }
        } catch (Throwable $e) {
            // Error handling
            $resultResponse->setStatusCode((int)$e->getCode());
            $resultResponse->setMessage($e->getMessage());
            $resultResponse->setData((object)["trace"=> $e->getTrace()]);
            report($e);
        }

        return response()->json($resultResponse);
    }
}
