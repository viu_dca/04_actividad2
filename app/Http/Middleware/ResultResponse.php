<?php

namespace App\Http\Middleware;

use PhpParser\Node\Expr\Cast\Object_;

/**
 * Handle the output response body.
 *
 */
class ResultResponse
{
    const SUCCESS_CODE = 200;
    const ERROR_CODE = 500;
    const ERROR_BAD_REQUEST_CODE = 400;
    const ERROR_ELEMENT_NOT_FOUND_CODE = 404;

    const TXT_SUCCESS_CODE = 'Success';
    const TXT_ERROR_CODE = 'Error';
    const TXT_ERROR_BAD_REQUEST_CODE = 'Bad request';
    const TXT_ERROR_ELEMENT_NOT_FOUND_CODE = 'Element not found';

    public int $statusCode;
    public string $message;
    public object $data;

    public function __construct() {
        $this->statusCode = self::ERROR_CODE;
        $this->message = self::TXT_ERROR_CODE;
        $this->data = (object)[];
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        return $this->data;
    }

    /**
     * @param object $data
     */
    public function setData(object $data): void
    {
        $this->data = $data;
    }
}
